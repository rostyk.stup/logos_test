import {httpGet, httpPost, httpPut, httpDelete} from './http-request-tool.js';

let shopsList = {};

document.addEventListener('DOMContentLoaded', function () {
    loadShops();
    setShopButtonOnClick();
});

document.addEventListener('DOMContentLoaded', function () {

});

const setShopButtonOnClick = () => {
    let createButton = document.getElementById('category-save-button');
    let categoryIdInput = document.getElementById('category-id-input');
    let categoryNameInput = document.getElementById('category-name-input');

    createButton.onclick = () => {
        if (!categoryIdInput.value) {
            const shop = {
                name: categoryNameInput.value
            }
            let request = httpPost('http://localhost:8080/shops', JSON.stringify(shop));
            if (request.status === 200) {
                categoryNameInput.value = '';
                loadShops();
            }
        } else {
            const shop = {
                name: categoryNameInput.value
            }
            let shopId = categoryIdInput.value;
            let request = httpPut('http://localhost:8080/shops/' + shopId, JSON.stringify(shop));
            if (request.status === 200) {
                categoryIdInput.value = '';
                categoryNameInput.value = '';
                createButton.innerText = 'Create';
                loadShops();
            }
        }
    }

}

const loadShops = () => {
    let request = httpGet("http://localhost:8080/shops");
    console.log(request);
    let response = JSON.parse(request.response);
    if (request.status === 200) {
        mapShopsFromResponse(response);
        addDetailsButtonOnClickActions();
        addEditButtonOnClickActions();
        addDeleteButtonOnClickActions();
    } else {
        console.error(response);
    }
}

const mapShopsFromResponse = (shops) => {
    appendCategoryTable();

    shopsList = {};
    let responseHtml = '';

    if (shops.length > 0) {
        for (let index in shops) {
            shopsList[shops[index].id] = shops[index];
            responseHtml += mapShopToHTML(shops[index]);
        }
    }
    let table = document.getElementById('shops-table');
    table.innerHTML = responseHtml;
}

const appendCategoryTable = () => {
    let container = document.getElementById('shops-table-container');
    container.innerHTML =
        `<table id="shops-table">
            <tr>id</tr>
            <tr>name</tr>
            <tr></tr>
            <tr></tr>
            <tr></tr>
        </table>`
}


const mapShopToHTML = (shop) => {
    return `
    <tr>
        <td>${shop.id}</td>
        <td>${shop.name}</td>
        <td><button class="shop-details-btn" shopid="${shop.id}">Details</button></td>
        <td><button class="shop-edit-btn" shopid="${shop.id}">Edit</button></td>
        <td><button class="shop-delete-btn" shopid="${shop.id}">Delete</button></td>
    </tr>
    `
}

const addDetailsButtonOnClickActions = () => {
    let elements = document.getElementsByClassName('shop-details-btn');
    for (let i = 0; i < elements.length; i++) {
      let button = elements[i];
      button.onclick = () => {
          console.log('button with shop id = ' + button.getAttribute('shopid'));
      }
    }
}

const addEditButtonOnClickActions = () => {
    let elements = document.getElementsByClassName('shop-edit-btn');
    for (let i = 0; i < elements.length; i++) {
      let button = elements[i];
      let shopId = button.getAttribute('shopid');
      button.onclick = () => {
          writeShopToCreateForm(shopsList[shopId]);
      }
    }
}

const writeShopToCreateForm = (shop) => {
    let categoryIdInput = document.getElementById('category-id-input');
    let categoryNameInput = document.getElementById('category-name-input');
    let createButton = document.getElementById('category-save-button');

    categoryIdInput.value = shop.id;
    categoryNameInput.value = shop.name;
    createButton.innerText = 'Save';
}


const addDeleteButtonOnClickActions = () => {
    let elements = document.getElementsByClassName('shop-delete-btn');
    for (let i = 0; i < elements.length; i++) {
      let button = elements[i];
      let shopId = button.getAttribute('shopid');
      button.onclick = () => {
          httpDelete("http://localhost:8080/shops/" + shopId);
          loadShops();
      }
    }
}

