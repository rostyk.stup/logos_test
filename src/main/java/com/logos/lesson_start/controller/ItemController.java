package com.logos.lesson_start.controller;

import com.logos.lesson_start.domain.Item;
import com.logos.lesson_start.dto.request.ItemRequestDTO;
import com.logos.lesson_start.dto.request.ItemSearchRequestDTO;
import com.logos.lesson_start.dto.response.ItemResponseDTO;
import com.logos.lesson_start.dto.response.PageResponse;
import com.logos.lesson_start.service.ItemService;
import com.logos.lesson_start.tools.FileTool;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import javax.validation.Valid;
import java.io.IOException;
import java.util.stream.Collectors;

@CrossOrigin
@RestController
@RequestMapping("/items")
public class ItemController {

	@Autowired
	private ItemService itemService;

	@Autowired
	private FileTool fileTool;

//	@PostMapping("/image")
//	private String saveImage(@RequestBody String image) throws IOException {
//		return fileTool.saveFile(image);
//	}

	// CRUD - create read update delete
	@PostMapping
	private void createItem(@RequestBody ItemRequestDTO item) throws IOException {
		itemService.save(item);
	}

	@PutMapping("/{id}")
	private ItemResponseDTO updateItem(@RequestBody ItemRequestDTO item, @PathVariable("id") Long id) throws IOException {
		return new ItemResponseDTO(itemService.update(item, id));
	}

	@GetMapping("/{id}")
	private ItemResponseDTO getById(@PathVariable("id") Long id) {
		return new ItemResponseDTO(itemService.getById(id));
	}

	@GetMapping
	private PageResponse<ItemResponseDTO> getAll(@Valid ItemSearchRequestDTO searchRequest) {
		System.out.println(searchRequest.getPagination());
		Page<Item> page = itemService.getPageByShopId(searchRequest);
		return new PageResponse<>(
				page.get()
						.map(ItemResponseDTO::new)
						.collect(Collectors.toList()),
				page.getTotalElements(),
				page.getTotalPages()
		);
	}

	@DeleteMapping("/{id}")
	private void deleteItem(@PathVariable("id") Long id) {
		itemService.delete(id);
	}
}
