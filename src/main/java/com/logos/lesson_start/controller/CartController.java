package com.logos.lesson_start.controller;

import com.logos.lesson_start.dto.response.CartResponseDTO;
import com.logos.lesson_start.service.CartService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@CrossOrigin
@RestController
@RequestMapping("/carts")
public class CartController {

	private CartService cartService;

	@Autowired
	public CartController(CartService cartService) {
		this.cartService = cartService;
	}

	@PutMapping("/add-item")
	private void addItemToCart(Long id, Long itemId) {
		cartService.addItem(id, itemId);
	}

	@GetMapping
	private CartResponseDTO getCart() {
		return new CartResponseDTO(cartService.getByCurrentUser());
	}
}
