package com.logos.lesson_start.controller;

import com.logos.lesson_start.dto.request.CategoryRequestDTO;
import com.logos.lesson_start.dto.response.CategoryResponseDTO;
import com.logos.lesson_start.service.CategoryService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;
import java.util.stream.Collectors;

@CrossOrigin
@RestController
@RequestMapping("/categories")
public class CategoryController {

	@Autowired
	private CategoryService categoryService;

	@PostMapping
	private void createCategory(@RequestBody CategoryRequestDTO categoryRequestDTO) {
		categoryService.create(categoryRequestDTO);
	}

	@GetMapping
	private List<CategoryResponseDTO> getAll() {
		return categoryService.getAll().stream()
				.map(CategoryResponseDTO::new)
				.collect(Collectors.toList());
	}
}
