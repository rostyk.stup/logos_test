package com.logos.lesson_start.controller;

import com.logos.lesson_start.dto.request.UserLoginRequestDTO;
import com.logos.lesson_start.dto.request.UserRegistrationRequestDTO;
import com.logos.lesson_start.dto.response.AuthenticationResponseDTO;
import com.logos.lesson_start.dto.response.UserResponseDTO;
import com.logos.lesson_start.service.UserServiceImpl;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@CrossOrigin
@RestController
@RequestMapping("/users")
public class UserController {

	private UserServiceImpl userServiceImpl;

	@Autowired
	public UserController(UserServiceImpl userServiceImpl) {
		this.userServiceImpl = userServiceImpl;
	}

	@PostMapping("/register")
	public AuthenticationResponseDTO registerUser(@RequestBody UserRegistrationRequestDTO requestDTO) {
		return userServiceImpl.registerUser(requestDTO);
	}

	@PostMapping("/login")
	public AuthenticationResponseDTO login(@RequestBody UserLoginRequestDTO requestDTO) {
		return userServiceImpl.login(requestDTO);
	}

}
