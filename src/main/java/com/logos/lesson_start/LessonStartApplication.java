package com.logos.lesson_start;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class LessonStartApplication {


	public static void main(String[] args) {
		SpringApplication.run(LessonStartApplication.class, args);
	}

//	TODO2 link to openapi
//	http://localhost:8080/v3/api-docs/

//	TODO5 link to swagger ui
//	http://localhost:8080/swagger-ui/index.html
}
