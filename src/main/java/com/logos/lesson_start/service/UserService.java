package com.logos.lesson_start.service;

import com.logos.lesson_start.domain.User;
import com.logos.lesson_start.dto.request.UserLoginRequestDTO;
import com.logos.lesson_start.dto.request.UserRegistrationRequestDTO;
import com.logos.lesson_start.dto.response.AuthenticationResponseDTO;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;

public interface UserService extends UserDetailsService {

	AuthenticationResponseDTO registerUser(UserRegistrationRequestDTO requestDTO);

	AuthenticationResponseDTO login(UserLoginRequestDTO requestDTO);

	User findByLogin(String username);

	User getCurrentUser();
}
