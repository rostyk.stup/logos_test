package com.logos.lesson_start.service;

import com.logos.lesson_start.domain.Item;
import com.logos.lesson_start.dto.request.ItemRequestDTO;
import com.logos.lesson_start.dto.request.ItemSearchRequestDTO;
import com.logos.lesson_start.dto.request.PaginationRequestDTO;
import org.springframework.data.domain.Page;

import javax.validation.constraints.NotNull;
import java.io.IOException;
import java.util.List;

public interface ItemService {

	void save(ItemRequestDTO item) throws IOException;

	Item getById(Long id);

	List<Item> getAll();

	Page<Item> getPageByShopId(@NotNull ItemSearchRequestDTO searchRequest);

	Item update(ItemRequestDTO item, Long id) throws IOException;

	void delete(Long id);
}
