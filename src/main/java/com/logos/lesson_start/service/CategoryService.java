package com.logos.lesson_start.service;

import com.logos.lesson_start.domain.Category;
import com.logos.lesson_start.dto.request.CategoryRequestDTO;

import java.util.List;

public interface CategoryService {

	void create(CategoryRequestDTO requestDTO);

	Category getById(Long id);

	List<Category> getAll();
}
