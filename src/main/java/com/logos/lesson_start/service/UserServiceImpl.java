package com.logos.lesson_start.service;

import com.logos.lesson_start.domain.Cart;
import com.logos.lesson_start.domain.User;
import com.logos.lesson_start.domain.UserRole;
import com.logos.lesson_start.dto.request.UserLoginRequestDTO;
import com.logos.lesson_start.dto.request.UserRegistrationRequestDTO;
import com.logos.lesson_start.dto.response.AuthenticationResponseDTO;
import com.logos.lesson_start.repository.UserRepository;
import com.logos.lesson_start.security.JwtTokenTool;
import com.logos.lesson_start.security.JwtUser;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.authentication.BadCredentialsException;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.stereotype.Service;

@Service
public class UserServiceImpl implements UserService {

	private UserRepository userRepository;

	private AuthenticationManager authenticationManager;

	private JwtTokenTool jwtTokenTool;

	private BCryptPasswordEncoder encoder;


	@Autowired
	public UserServiceImpl(UserRepository userRepository,
						   AuthenticationManager authenticationManager,
						   JwtTokenTool jwtTokenTool,
						   BCryptPasswordEncoder encoder) {
		this.userRepository = userRepository;
		this.authenticationManager = authenticationManager;
		this.jwtTokenTool = jwtTokenTool;
		this.encoder = encoder;
	}

	@Override
	public AuthenticationResponseDTO registerUser(UserRegistrationRequestDTO request) {
		if (userRepository.existsByLogin(request.getLogin())) {
			throw new BadCredentialsException(
					"User with login " + request.getLogin() + " already exists");
		}
		User user = new User();
		user.setLogin(request.getLogin());
		user.setUserRole(UserRole.ROLE_USER);
		user.setPassword(encoder.encode(request.getPassword()));
		user.setUsername(request.getUsername());
		user.setCart(new Cart());

		userRepository.save(user);

		return login(mapRegistrationToLogin(request));
	}

	@Override
	public AuthenticationResponseDTO login(UserLoginRequestDTO request) {
		String login = request.getLogin();
		User user = findByLogin(login);

		authenticationManager.authenticate(new UsernamePasswordAuthenticationToken(login, request.getPassword()));

		String token = jwtTokenTool.createToken(login, user.getUserRole());
		String name = user.getUsername();
		Long id = user.getId();

		return new AuthenticationResponseDTO(name, token, id);
	}

	private UserLoginRequestDTO mapRegistrationToLogin(UserRegistrationRequestDTO registration) {
		return new UserLoginRequestDTO(registration.getLogin(), registration.getPassword());
	}

	@Override
	public User findByLogin(String username) {
		return userRepository.findByLogin(username);
	}

	@Override
	public User getCurrentUser() {
		return findByLogin((String) SecurityContextHolder.getContext().getAuthentication().getPrincipal());
	}

	@Override
	public UserDetails loadUserByUsername(String login) throws UsernameNotFoundException {
		User user = userRepository.findByLogin(login);
		return new JwtUser(user.getLogin(), user.getUserRole(), user.getPassword());
	}
}

