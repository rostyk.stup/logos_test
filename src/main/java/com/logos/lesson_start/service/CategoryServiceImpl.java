package com.logos.lesson_start.service;

import com.logos.lesson_start.domain.Category;
import com.logos.lesson_start.dto.request.CategoryRequestDTO;
import com.logos.lesson_start.repository.CategoryRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class CategoryServiceImpl implements CategoryService {

	private CategoryRepository categoryRepository;

	@Autowired
	public CategoryServiceImpl(CategoryRepository categoryRepository) {
		this.categoryRepository = categoryRepository;
	}

	@Override
	public void create(CategoryRequestDTO requestDTO) {
		categoryRepository.save(mapCategoryRequestDTOToCategory(requestDTO));
	}

	@Override
	public Category getById(Long id) {
		return categoryRepository.findById(id).orElseThrow(
				() -> new IllegalArgumentException("Category with id " + id + " doesn't exist")
		);
	}

	@Override
	public List<Category> getAll() {
		return categoryRepository.findAll();
	}

	private Category mapCategoryRequestDTOToCategory(CategoryRequestDTO categoryRequestDTO) {
		Category category = new Category();
		category.setName(categoryRequestDTO.getName());

		return category;
	}
}
