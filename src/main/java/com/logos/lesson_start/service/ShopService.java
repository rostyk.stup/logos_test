package com.logos.lesson_start.service;

import com.logos.lesson_start.domain.Shop;
import com.logos.lesson_start.dto.request.PaginationRequestDTO;
import com.logos.lesson_start.dto.request.ShopRequestDTO;
import org.springframework.data.domain.Page;

import java.util.List;

public interface ShopService {

	Shop save(ShopRequestDTO shop);

	Shop update(ShopRequestDTO shop, Long id);

	Shop getById(Long id);

	List<Shop> getAll();

	List<Shop> getAllByCategory(Long categoryId);

	Page<Shop> getPage(PaginationRequestDTO paginationRequestDTO);

	void delete(Long id);
}
