package com.logos.lesson_start.domain;

public enum EntityStatus {
	ACTIVE, DELETED
}
