package com.logos.lesson_start.repository;

import com.logos.lesson_start.domain.Cart;
import com.logos.lesson_start.domain.Item;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

@Repository
public interface CartRepository extends JpaRepository<Cart, Long> {

	@Query(
			"select case when count(KEY(items))> 0 then true else false end " +
					"from Cart c join c.itemsCountMap items " +
					"where c =:cart and KEY(items).id = :itemId"
	)
	boolean cartContainsItem(
			@Param("cart") Cart cart,
			@Param("itemId") Long item
	);

}
