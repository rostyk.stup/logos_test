package com.logos.lesson_start.repository;

import com.logos.lesson_start.domain.User;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.stereotype.Repository;

@Repository
public interface UserRepository extends JpaRepository<User, Long> {

	boolean existsByLogin(String login);

	User findByLogin(String login);
}
