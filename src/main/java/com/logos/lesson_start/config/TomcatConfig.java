package com.logos.lesson_start.config;

import org.apache.tomcat.util.http.LegacyCookieProcessor;
import org.springframework.boot.web.embedded.tomcat.TomcatServletWebServerFactory;
import org.springframework.boot.web.server.WebServerFactory;
import org.springframework.boot.web.server.WebServerFactoryCustomizer;
import org.springframework.context.annotation.Configuration;

@Configuration
public class TomcatConfig implements WebServerFactoryCustomizer {

	@Override
	public void customize(WebServerFactory factory) {
		((TomcatServletWebServerFactory)factory).addContextCustomizers(
				context -> context.setCookieProcessor(new LegacyCookieProcessor())
		);
	}

}
