package com.logos.lesson_start.specification;

import com.logos.lesson_start.domain.Item;
import com.logos.lesson_start.domain.Shop;
import com.logos.lesson_start.dto.request.ItemSearchRequestDTO;
import org.springframework.data.jpa.domain.Specification;

import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Join;
import javax.persistence.criteria.Predicate;
import javax.persistence.criteria.Root;
import java.util.ArrayList;
import java.util.List;

public class ItemSpecification implements Specification<Item> {

	private String name;
	private Integer maxPrice;
	private Integer minPrice;
	private Integer count;
	private Integer shopId;

	// Конструктор для зручності
	public ItemSpecification(ItemSearchRequestDTO itemSearchRequest) {
		this.name = itemSearchRequest.getName();
		this.count = itemSearchRequest.getCount();
		this.maxPrice = itemSearchRequest.getMaxPrice();
		this.minPrice = itemSearchRequest.getMinPrice();
		this.shopId = itemSearchRequest.getShopId();
	}

	@Override
	public Predicate toPredicate(Root<Item> root, CriteriaQuery<?> query, CriteriaBuilder cb) {
		List<Predicate> predicates = new ArrayList<>();

		predicates.add(findByPrice(root, cb));
		predicates.add(findByCount(root, cb));
		predicates.add(findByName(root, cb));
		predicates.add(findByShop(root, cb));

		return cb.and(predicates.toArray(new Predicate[0]));
	}

	private Predicate findByPrice(Root<Item> root, CriteriaBuilder cb) {
		Predicate predicate;

		if (minPrice == null && maxPrice == null) {
			predicate = cb.conjunction(); // skip price params
		} else if (minPrice == null) {
			// less then or equal to maxPrice ( <= maxPrice)
			predicate = cb.lessThanOrEqualTo(root.get("price"), maxPrice);
		} else if (maxPrice == null) {
			// greater then or equal to minPrice ( >= minPrice)
			predicate = cb.greaterThanOrEqualTo(root.get("price"), minPrice);
		} else {
			predicate = cb.between(root.get("price"), minPrice, maxPrice);
		}
		return predicate;
	}

	private Predicate findByCount(Root<Item> root, CriteriaBuilder cb) {
		Predicate predicate;

		if (count == null) {
			predicate = cb.conjunction(); // skip price params
		} else {
			predicate = cb.greaterThanOrEqualTo(root.get("count"), count);
		}
		return predicate;
	}

	private Predicate findByName(Root<Item> root, CriteriaBuilder cb) {
		Predicate predicate;
		if (name == null) {
			predicate = cb.conjunction(); // skip price params
		} else {
			predicate = cb.like(root.get("name"), '%' + name + '%');
		}

		return predicate;
	}

	private Predicate findByShop(Root<Item> root, CriteriaBuilder cb) {

		final Join<Item, Shop> shopJoin = root.join("shop"); // join
		return cb.equal(shopJoin.get("id"), shopId);

	}


}
