package com.logos.lesson_start.dto.request;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class UserRequestDTO {

	private String login;

	private String password;

	private String username;

}
