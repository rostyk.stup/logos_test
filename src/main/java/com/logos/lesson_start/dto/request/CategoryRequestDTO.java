package com.logos.lesson_start.dto.request;

import lombok.Getter;

@Getter
public class CategoryRequestDTO {

	private String name;

}
