package com.logos.lesson_start.dto.response;

import com.logos.lesson_start.domain.Shop;
import lombok.Getter;
import lombok.Setter;

import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;

@Getter
@Setter
public class ShopResponseDTO {

	private Long id;

	private String name;

	private List<CategoryResponseDTO> categories = new ArrayList<>();

	public ShopResponseDTO(Shop shop) {
		this.id = shop.getId();
		this.name = shop.getName();
		if (shop.getCategories() != null) {
			this.categories = shop.getCategories()
					.stream()
					.map(CategoryResponseDTO::new)
					.collect(Collectors.toList());
		}
	}
}
