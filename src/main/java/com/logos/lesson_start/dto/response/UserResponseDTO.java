package com.logos.lesson_start.dto.response;

import com.logos.lesson_start.domain.User;
import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class UserResponseDTO {

	private String username;

	public UserResponseDTO(User user) {
		this.username = user.getUsername();
	}
}
